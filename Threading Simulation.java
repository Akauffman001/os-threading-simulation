//Author: Adam Kauffman
//OS simulation
/*
This is an OS Simulation of Treading Impact exercise which you can use java to simulate. Following are the rules:
Create a multidimensional array with rows and columns, as shown in the SS template:
You need to create 5000 random records with these fields.
Priority 1 gets processed first / 10 last
Echelon 1 gets the first precedence, then the priority
For process simulations use the Sleep Clause with a Fixed Second
Process the Jobs in one Machine Instance (Thread) and record begin and end time to calculate total processing time
Process the Jobs in two Machines Machine Instances (Threads) and record begin and end time to calculate total processing time in both instances.
When simulations are executed in multi-thread, theoretically (simulated), force each task to take a random of (20%-80%) more time in each task.
Record each task processing time, print a report in a file (log) by each task processing time and within a Single and the Double Threads with Total Processing Times. 
I shall check the execution and read the output file along with your program.
*/
package newsim;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class Newsim {
public static void main(String args[])throws IOException, InterruptedException {
int w = 2499,
msTotal=0,//accumulator for overall time taken
Etotal=0,//temp storage for start time per ech group
stime=0,//temp storage for start time per ech group
compare=1,
q=0,// indexer for mass array for storing ran gen array info
t=1;//trigger/switch to determine if first time in group
            
int E0[][]=new int[5000][4];  //mass array
int E1[][] =new int [2500][4];
int E2[][]=new int[2500][4];
int Array[][]=new int[5][4];            //temp array for ran info                          //
int Stime[] = new int [5000];             //start time array storage
int Etimes[] = new int [5000];            //end time array storage
int Displacement[] = new int [5000];      //displacement
BufferedWriter bw = null;

try {     
String m1 = "----------------------Machine 1-----------------------";
String m2 = "----------------------Machine 2----------------------";
String single = "----------------------Single Thread----------------------";
String startT = "START TIME:"; 
String endT = "END TIME:";
String displacementT = "Processing Cycle:";
String mycontent = "ECHELON PRI \tREQ \tSTART \tEND \tProcessing Time(ms)";  // heading for each array 
File file = new File("D:/NEWlog.txt");       //location and text file
FileWriter fw = new FileWriter(file);
bw = new BufferedWriter(fw);   
    if (!file.exists()) { //This logic will make sure that the file gets created if it is not present at the specified location
        file.createNewFile();   
    }
bw.write(single);
bw.newLine();
bw.write(mycontent);                        //write heading
bw.newLine(); 
    for(int rec=0; rec<1000; rec++){   
        Array = getarr(Array); //get array of req ran vals, sort it
        organize(Array);    
        for(int i=0; i<5; i++){
            System.arraycopy(Array[i], 0, E0[q], 0, 4); //populate array w/ ran vals
            q++; //index for larger array
        }
        if(rec==999){
            for(int i=0;i<2500;i++){
                System.arraycopy(E0[i], 0, E1[i], 0, 4); 
                System.arraycopy(E0[w], 0, E2[i], 0, 4);
                w++;
            }
            massorganize(E0);
            miniorganize(E1);
            miniorganize(E2);
        } 
    }    
for(int j=0;j<5000;j++){                               //writes in Echelon groups
    for(int i=0;i<4;i++){
        if(E0[j][i]==E0[j][0] && E0[j][0]!=compare){                                                          //begin new ech group, writes times to start new group
                bw.write("TOTAL:\t\t\t\t\t"+Etotal);
                msTotal+=Etotal;
                Etotal=0;
                bw.newLine();
                bw.write(startT + stime); 
                t++;                                //reset swtich 
                bw.newLine();
                bw.write(endT + Etimes[j-1]);
                bw.newLine();
                bw.write(E0[j][i] +"\t");
                compare++; //if dealing w/ smaller logs, should be in conditional to check if next ech group inc by 1,2,3, or 4
        }else if(E0[j][i] == E0[j][3] && i==3){ //if time
                if(i==3 && t==1){          //switch t: determine if first time in new ech group
                  stime=E0[j][i];
                  Stime[j]=E0[j][i];
                  t--;
                }
               Etimes[j]=E0[j][i]+(int)(Math.random() * 1000 + 1); 
               Displacement[j] = Etimes[j]-E0[j][i];
               bw.write(E0[j][i] +"\t"+Etimes[j]+"\t"+Displacement[j]);
               Etotal+=Displacement[j];
        }else if(E0[j][i]==E0[j][1]||E0[j][i]==E0[j][2]||E0[j][0]==compare) {//if not time or echelon that starts new group
                bw.write(E0[j][i] +"\t");
            }   
    } 
    bw.newLine();
    if (j==4999){
       bw.write("TOTAL:\t\t\t\t\t"+Etotal);
       msTotal+=Etotal;
       bw.newLine();
       bw.write(startT + stime); 
       bw.newLine();
       bw.write(endT + Etimes[j]);
       bw.newLine();
       bw.write("PROCESSING CYCLE:\t"+Stime[0]+"\t"+Etimes[j]+"\t"+msTotal);
       bw.newLine(); 
       bw.flush();
       msTotal=0;// reset accumulator for multiThread
    }
}
System.out.println("Single Thread written Successfully");
w=0;          
for(int i=0;i<5000;i++){
   if(i>2499){
     System.arraycopy(E2[w], 0, E0[i], 0, 4); 
     w++;
    }else{
         System.arraycopy(E1[i], 0, E0[i], 0, 4);
        }
}
bw.write("---------------------Multi-Thread---------------------");
bw.newLine();
bw.write(m1);
bw.newLine();
bw.write(mycontent);                        //write heading
bw.newLine(); 
compare=1;
t=1;//trigger/switch to determine if first time in group
int x=2;//indexer could be replaced with j, but x-1 is better.
for(int j=0;j<5000;j++){                               //writes in Echelon groups
    for(int i=0;i<4;i++){
        if(E0[j][i]==E0[j][0] && E0[j][0]!=compare){                                                        //begin new ech group, writes times to start new group
                bw.write("TOTAL:\t\t\t\t\t"+Etotal);
                msTotal+=Etotal;
                Etotal=0;
                bw.newLine();
                bw.write(startT + stime); 
                t++;                                //reset swtich 
                bw.newLine();
                bw.write(endT + Etimes[j-1]);
                bw.newLine();
                bw.write(E0[j][i] +"\t");
                compare++;
        }else if(E0[j][i] == E0[j][3] && i==3){ //if time
                if(i==3 && t==1){          //switch t: determine if first time in group
                   stime=E0[j][i];
                   Stime[j]=E0[j][i];
                   t--;
                }
                 Etimes[j]=(int) (E0[j][i]+((int)(Math.random() * 1000 + 1))+((int)(Math.random() * 1000 + 1)*((Math.random() * (80 - 20) + 20) / 100))); //tried literally everything couldnt get multi-threads to be 20-80% more time
                 Displacement[j] = Etimes[j]-E0[j][i];                                                                                                      //unsure why Etimes[j]+= Displacement[j]+*((Math.random() * (80 - 20) + 20) / 100)) would give me negatives
                 bw.write(E0[j][i] +"\t"+Etimes[j]+"\t"+Displacement[j]);                                                                                   // shouldnt be hard, i just spent alot of time and nothing was working out. closest i could get
                 Etotal+=Displacement[j];
            }else if(E0[j][i]==E0[j][1]||E0[j][i]==E0[j][2]||E0[j][0]==compare){//if not time or echelon that starts new group
                     bw.write(E0[j][i] +"\t");
                }   
    } 
    bw.newLine();
    if (j==2499||j==4999){
        bw.write("TOTAL:\t\t\t\t\t"+Etotal);
        msTotal+=Etotal;
        bw.newLine();
        bw.write(startT + stime); 
        bw.newLine();
        bw.write(endT + Etimes[j]);
        bw.newLine();
        bw.write("PROCESSING CYCLE:\t"+Stime[0]+"\t"+Etimes[j]+"\t"+msTotal);
        bw.newLine(); 
        bw.flush();
        if(x==2){
           compare=1;
           t=1;//trigger/switch to determine if first time in group
           System.out.println("Machine 1 written Successfully");
           bw.write(m2);
           bw.newLine();
           bw.write(mycontent);                        //write heading
           bw.newLine(); 
        }else{ 
            System.out.println("Machine 2 written Successfully");
            }
        x--;
    }
}
 
}catch (IOException ioe) {                          //if error with writing 
     System.out.println("Error"); 
    }
finally{                                            //if error closing writter
    try{
        if(bw!=null)
           bw.close();
    }catch(IOException ex){
	   System.out.println("Error in closing the BufferedWriter"+ex);
	}
} 
    }//close main method
        
        
private static int[][] getarr(int[][] input) { //method for populating array                     
int[][] nArray = new int[5][4];
for(int j=0;j<4;j++){                              //populate array w/ ran vals
    for(int i=0;i<5;i++){
        switch(j){
               case 0: nArray[i][j]= (int)(Math.random() * 10 + 1);
                    break;
               case 1: nArray[i][j]= (int)(Math.random() * 1000 + 1);
                    break;
               case 2: nArray[i][j]= (int)(Math.random() * 10000 + 1);
                    break;
               case 3: nArray[i][j]= (int)(Math.random() * 5 + 1);
                                break; 
        }
    }         
}
return nArray;  
}    
private static int[][] organize(int[][] nArray) { //method for sorting array
int x =1;
int y =1;
int temp[][]=new int[5][4];
Arrays.sort(nArray, (a, b) -> Integer.compare(a[3], b[3])); //sort row by Echelon, lowest to greatest
    for(int i=0;i<5;i++){
        System.arraycopy(nArray[i], 0, temp[i], 0, 4); 
    }                  
Arrays.sort(nArray, (a, b) -> Integer.compare(a[0], b[0])); //sort priority by lowest to greatest
for(int i=0;i<5;i++){
    for(int j=0;j<4;j++){
        if(j==3){
           nArray[i][j]=temp[i][j];  
        }
    }
}           
for (int c = 0; c<5; c++){                        //1/2 of right shift so Echelon has higher precedence
     int p =4;
     for (int s = 0; s<4;s++){
          p--;
          temp[c][s] = nArray[c][s];
          nArray[c][s] = nArray[c][p];                        //duplicates lines
     }
}
for(int c = 0; c<5; c++){//finishing half of right switch
    int p =4;
    for (int s = 1; s<4;s++){
         p--;
         nArray[c][s] = temp[c][s-1];//restores needed lines.
    } 
}
return nArray;              //return populted, sorted array
}
private static int[][] massorganize(int[][] nArray) { //method for sorting array
int x =1;
int y =1;
int temp[][]=new int[5000][4];
Arrays.sort(nArray, (a, b) -> Integer.compare(a[0], b[0])); //sort row by Echelon, lowest to greatest
for(int i=0;i<5000;i++){
    System.arraycopy(nArray[i], 0, temp[i], 0, 4); 
}                  
Arrays.sort(nArray, (a, b) -> Integer.compare(a[1], b[1])); //sort priority by lowest to greatest
for(int i=0;i<5000;i++){
    for(int j=0;j<4;j++){
        if(j==0){                                                //to restore echelon order
           nArray[i][j]=temp[i][j];  
        }
    }
} 
return nArray;              //return populted, sorted array
}
private static int[][] miniorganize(int[][] nArray) { //method for sorting array
int temp[][]=new int[2500][4];
Arrays.sort(nArray, (a, b) -> Integer.compare(a[0], b[0])); //sort row by Echelon, lowest to greatest
for(int i=0;i<2500;i++){
    System.arraycopy(nArray[i], 0, temp[i], 0, 4); 
}                  
Arrays.sort(nArray, (a, b) -> Integer.compare(a[1], b[1])); //sort priority by lowest to greatest  
for(int i=0;i<2500;i++){
    for(int j=0;j<4;j++){
        if(j==0){                                                //to restore echelon order
           nArray[i][j]=temp[i][j];  
        }
    }
} 
return nArray;              //return populted, sorted array
}
}
